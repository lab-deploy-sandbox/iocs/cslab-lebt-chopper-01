#!/usr/bin/env iocsh.bash
###############################################################################
#
# EPICS startup script to launch IOC using modules to control devices that are
#   part of LEBT Chopper system
# -----------------------------------------------------------------------------
# Devices being controlled:
#   - XT Pico 
#   - PicoScope 4824
#   - FuG Power Supply HCP 35-35000
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2020
# -----------------------------------------------------------------------------
# WP10 - antoni.simelio@ess.eu
# WP12 - douglas.bezerra.beniz@ess.eu
###############################################################################

require(xtpico,0.9.0)
require(adcore, 3.9.0)
require(admisc, 2.1.1)
require(asyn, 4.37.0)
require(busy, 1.7.2_c596e1c)
require(adps4000a, master)
require(fug, master)
require(stream, 2.8.10)

#
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

# -----------------------------------------------------------------------------
# ---------------------------------- General ----------------------------------
# -----------------------------------------------------------------------------
epicsEnvSet("LOCATION",  "CSLab")

# -----------------------------------------------------------------------------
# ---------------------------------- XT Pico ----------------------------------
# -----------------------------------------------------------------------------
# Prefix for all records
epicsEnvSet("DEVICE_NAME_PU",  "PwrC-RepPS-01:")
epicsEnvSet("PREFIX_XT",        "$(LOCATION)-XTPICO-01")
epicsEnvSet("DEVICE_IP_XT",     "172.30.5.249")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")
epicsEnvSet("I2C_TCA9555_PORT", "AK_I2C_TCA9555")
epicsEnvSet("I2C_TMP100_PORT",  "AK_I2C_TMP100")
epicsEnvSet("I2C_LTC2991_PORT", "AK_I2C_LTC2991")
epicsEnvSet("I2C_ADT7420_PORT", "AK_I2C_ADT7420")

# -----------------------------------------------------------------------------
#- Create the asyn port to talk XTpico server on TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP_XT):1002")

# -----------------------------------------------------------------------------
# Debug
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

# -----------------------------------------------------------------------------
# Port Expander - TCA9555
# -----------------------------------------------------------------------------
# AKI2CTCA9555Configure(const char *portName, const char *ipPort,
#        int devCount, const char *devInfos, int priority, int stackSize);
AKI2CTCA9555Configure($(I2C_TCA9555_PORT), $(I2C_COMM_PORT), 1, "0x21", 1, 0, 0)
dbLoadRecords("AKI2C_TCA9555.db", "P=$(PREFIX_XT),R=:I2C1:IOExp1:,PORT=$(I2C_TCA9555_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# ADT/TMP temperature sensor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:I2C1:Temp1:, COUNT=1, INFOS=0x49")
#AKI2CTMP100Configure("TMP100.1", "AK_I2C_COMM", 1, "0x49", 1, 0, 0)
#dbLoadRecords("AKI2C_TMP100.db", "P=$(PREFIX_XT), R=:I2C1:Temp1:, PORT=TMP100.1, IP_PORT=AK_I2C_COMM, ADDR=0, TIMEOUT=1")
AKI2CADT7420Configure($(I2C_ADT7420_PORT), $(I2C_COMM_PORT), 1, "0x49", 1, 0, 0)
dbLoadRecords("AKI2C_ADT7420.db", "P=$(PREFIX_XT),R=:I2C1:Temp1:,PORT=$(I2C_ADT7420_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# TMP100
# -----------------------------------------------------------------------------
#AKI2CTMP100Configure($(I2C_TMP100_PORT), $(I2C_COMM_PORT), 1, "0x49", 1, 0, 0)
#dbLoadRecords("AKI2C_TMP100.db",       "P=$(PREFIX_XT),R=:I2C1:Temp2:,PORT=$(I2C_TMP100_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# Voltage and current monitor
# -----------------------------------------------------------------------------
#iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=2, NAME=:I2C1:VMon1:, COUNT=1, INFOS=0x90")
AKI2CLTC2991Configure($(I2C_LTC2991_PORT), $(I2C_COMM_PORT), 1, "0x48", 0, 0, 1, 0, 0)
dbLoadRecords("AKI2C_LTC2991.db", "P=$(PREFIX_XT),R=:I2C1:VMon1:,PORT=$(I2C_LTC2991_PORT),IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")

# -----------------------------------------------------------------------------
# --------------------------------- PicoScope ---------------------------------
# -----------------------------------------------------------------------------
#- 10 MB max CA request
epicsEnvSet("DEVICE_NAME_PS",               "PS4824-01")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX_PS",                    "$(LOCATION)-$(DEVICE_NAME_PS):")
epicsEnvSet("PORT",                         "PICO")
# epicsEnvSet("NUM_SAMPLES",                  "1000")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- create a PicoScope 4000A driver
#- PS4000AConfig(const char *portName, int numSamples, int dataType,
#-               int maxBuffers, int maxMemory, int priority, int stackSize)
#- dataType == NDInt32 == 4
PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

dbLoadRecords("ps4000a.template","P=$(PREFIX_PS),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")

#- individual input channels
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=0, NAME=A")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=1, NAME=B")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=2, NAME=C")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=3, NAME=D")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=4, NAME=E")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=5, NAME=F")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=6, NAME=G")
iocshLoad("$(adps4000a_DIR)channel.iocsh", "PREFIX=$(PREFIX_PS"), ADDR=7, NAME=H")

# -----------------------------------------------------------------------------
# ----------------------------------- PS FUG ----------------------------------
# -----------------------------------------------------------------------------
epicsEnvSet("DEVICE_NAME_PU",  "PwrC-RepPS-01:")
epicsEnvSet("PREFIX_PU",       "$(LOCATION)-$(DEVICE_NAME_PU)")
#epicsEnvSet("DEVICE_IP_PU",    "172.30.150.179")

#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=$(PREFIX_PU), IP_addr=$(DEVICE_IP_PU), P=$(LOCATION)-, R=$(DEVICE_NAME_PU)")

iocInit()

# -----------------------------------------------------------------------------
# Set some default configurations
# -----------------------------------------------------------------------------
# Enables AD ArrayCallbacks for PicoScope device
dbpf $(PREFIX_PS)ArrayCallbacks 1
# Enables channels A-D:s Callbacks then
dbpf $(PREFIX_PS)A-Enabled 1
dbpf $(PREFIX_PS)B-Enabled 1
dbpf $(PREFIX_PS)C-Enabled 1
dbpf $(PREFIX_PS)D-Enabled 1

# Set EGUs for temperature and voltage monitors via ADT7420 and LTC2991
dbpf $(PREFIX_XT):I2C1:Temp1:Value_RBV.EGU ".oC"
dbpf $(PREFIX_XT):I2C1:VMon1:ValueV1_RBV.EGU "V"
dbpf $(PREFIX_XT):I2C1:VMon1:ValueV3_RBV.EGU "V"
dbpf $(PREFIX_XT):I2C1:VMon1:ValueV3_RBV.EGU "V"
dbpf $(PREFIX_XT):I2C1:VMon1:ValueV4_RBV.EGU "V"
# Set factor for voltage monitors via LTC2991
dbpf $(PREFIX_XT):I2C1:VMon1:FactorV1 2.0
dbpf $(PREFIX_XT):I2C1:VMon1:FactorV2 2.0
dbpf $(PREFIX_XT):I2C1:VMon1:FactorV3 2.0
dbpf $(PREFIX_XT):I2C1:VMon1:FactorV4 2.0

